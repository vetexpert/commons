package com.vetexpert.utils

import java.util.UUID

fun String.uuid() = UUID.fromString(this)

val uuidRegexp = "[a-f0-9]{8}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{12}".toRegex()

fun String.findAllUuids(): Set<UUID> = uuidRegexp.findAll(this).map { it.value.uuid() }.toSet()
