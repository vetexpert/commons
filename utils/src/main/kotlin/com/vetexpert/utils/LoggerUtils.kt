package com.vetexpert.utils

import org.slf4j.Logger
import org.slf4j.LoggerFactory

val logger: Logger = LoggerFactory.getLogger("")

fun Any.hide() = this.takeIf { logger.isTraceEnabled } ?: "***"
