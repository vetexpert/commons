import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    kotlin("jvm") version "1.5.21"
    kotlin("plugin.spring") version "1.5.21"
    id("com.google.protobuf") version "0.8.17" apply false
}

group = "com.vetexpert"
rootProject.version = "0.0.1-2"
java.sourceCompatibility = JavaVersion.VERSION_11

repositories {
    repositories {
        mavenCentral()
        mavenLocal()
        maven("https://plugins.gradle.org/m2/")
        maven {
            name = "Gitlab"
            url = uri("https://gitlab.com/api/v4/groups/vetexpert/-/packages/maven")
            credentials(HttpHeaderCredentials::class) {
                name = "Job-Token"
                value = System.getenv("CI_JOB_TOKEN")
            }
            authentication {
                create<HttpHeaderAuthentication>("header")
            }
        }
    }
}

dependencies {
    implementation("org.jetbrains.kotlin:kotlin-reflect")
    implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")
}

tasks.withType<KotlinCompile> {
    kotlinOptions {
        freeCompilerArgs = listOf("-Xjsr305=strict")
        jvmTarget = "11"
    }
}

tasks.withType<Test> {
    useJUnitPlatform()
}
