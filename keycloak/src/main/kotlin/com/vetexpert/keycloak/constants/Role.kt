package com.vetexpert.keycloak.constants

enum class Role {
    DOCTOR,
    ASSISTANT,
    ADMINISTRATOR,
    MANAGEMENT,
    DIRECTOR
}
